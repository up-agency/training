<h1>Types and `undefined`: The Edges of Our Programs</h1>

We have types to help protect us from making mistakes, and ultimately, 
see less bugs from "Cannot read property 'x' of undefined" errors in production,
catching them before they ever get there.

The riskiest parts of our systems to to be around the edges: getting data from REST APIs, 
information from forms, that kind of thing.

Remember: writing TypeScript is not about trying to silence the compiler errors any way we can,
but understanding their guidance, and using them to build less buggy code.

If we tell TypeScript that something come from an API is a string, it has to believe us, 
because it has no way of knowing that this is true. At run-time, when our code gets
converted into JavaScript, all our types will disappear.

This means it's important to be especialy careful with types at the edges of our programs.

<h2>Example: [index.ts](index.ts)</h2>

This example doesn't use type casting, but the principles apply doubly there too.

There's a deliberate bug in this program. `firstName` can come back as `undefined`
in the API response, but that isn't reflected in the type:

```ts
interface Person { firstName: string; lastName: string; }
```

When you try and run this program it will error with the message: 

    Cannot read property 'length' of undefined

The options here are:

1. Specify a sensible default. Almost always the best option if it makes sense in your use-case. Here's an example:
  https://www.typescriptlang.org/docs/handbook/advanced-types.html#type-guards-and-type-assertions

2. Throw an exception if you can't massage it into the correct type (useful if there's no way for your program
to continue without this information in the type it expects)

3. Tell TypeScript it might be undefined: 

```ts
interface Name { name: string|undefined }

// or with the clearer syntax:
interface Name { name?: string }
```

This is 
the least desirable choice as now everywhere this gets used, someone is going to need to check 
if it's defined before using it, but it's definitely better than nothing.

Aside: I learned something interesting. TS will *not*  protect
you against outputting `"undefined Prendergast"` as it will happily
pass `undefined` into string coersion. This is something under active
discussion: https://github.com/microsoft/TypeScript/issues/30239

That's another reason to always use sensible defaults where you can.

