/**
this program will compile ok, but throw 
`UnhandledPromiseRejectionWarning: TypeError: Cannot read property 'length' of undefined`
when run because the remote api will return undefined for firstName, but our types don't reflect that
*/
import axios, {AxiosResponse} from "axios";

                   // can actually be `undefined`
axios
  .get("https://httpbin.org/anything?lastName=Prendergast")
  .then(response => {
    const person = responseToPerson(response);

    // this will throw UnhandledPromiseRejectionWarning: TypeError: Cannot read property 'length' of undefined
    const fullName = {fullName: nameFor(person) + ' ' + person.lastName}

    sendResponse(
      {...person, ...fullName}
    )
});


interface Person { firstName: string; lastName: string; }
interface FullName { fullName: string }
type Response = FullName & Person

/* stand-in for express' res.send(...), but instead
of sending it back to a web browser it just prints it out
on the console */
function sendResponse(response: Response): void {
  console.log(response);
}


/* some utilities for working with Person */
function responseToPerson(response: AxiosResponse): Person {
  return {
    firstName: response.data.args.firstName,
    lastName: response.data.args.lastName
  };
}

function nameFor(person: Person) {
  const firstName = truncFirstname(person.firstName);
  return `${firstName} ${person.lastName}`;
}

function truncFirstname(firstName: string){
  // Throws Cannot read property 'length' of undefined

  // If we'd specified that firstName is optional, TypeScript would
  // have forced us to check if firstname was undefined first and
  // the bug would never have made it to production
  if(firstName.length > 15){
    return firstName.substr(0, 15) + '...';
  }else{
    return firstName;
  }
}
