import * as readline from 'readline';

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('Your age, sir? ', (answer) => {
  // if it's really too complex, put it in a function and call it where it's needed
  const canDrink = (age: string) : boolean => {
    if(Number(age) > 18){
      return true;
    }else{
      return false;
    }

    // aside: you may have heard "ifs are bad".
    // trying to obsfuscate ifs is worse
  }

  console.log("You may drink: ", canDrink(answer));
  process.exit(0);
});

// Next chapter:
// How to Break Down Complexity 
//
// This will address:
//
// "Now my file is just a bunch of unconnected functions floating around, 
// a mix of objects, procural, and functional. Is there a better way?"




