import * as readline from 'readline';

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('Your age, sir? ', (answer) => {
  // if it's boolean, just use what you've used in the if statement
  const canDrink = Number(answer) > 18;

  console.log("You may drink: ", canDrink);
  process.exit(0);
});

