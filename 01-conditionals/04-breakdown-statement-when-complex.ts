import * as readline from 'readline';

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('Your age, sir? ', (answer) => {
  // If you need to check more conditions, and the contents of your check starts getting big and ugly, put it in a variable first, or multiple variables
  const age = Number(answer);
  const canDrink = age > 18;

  console.log("You may drink: ", canDrink);
  process.exit(0);
});

