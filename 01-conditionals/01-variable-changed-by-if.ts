import * as readline from 'readline';

// Why don't we use C to make web applicaitons?

// How do you feel when your house is a mess? 
// How do you feel when it's tidy?

// What's the difference between let and const?
// Why did language designers think const would be useful?

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('Your age, sir? ', (answer) => {
  let canDrink = false;
  if(Number(answer) > 18)
    canDrink = true;

  console.log("You may drink: ", canDrink);
  process.exit(0);
});


