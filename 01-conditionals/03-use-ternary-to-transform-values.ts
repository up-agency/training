import * as readline from 'readline';

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.question('Your age, sir? ', (answer) => {
  // If you need to transform values, but it's still simple, use a ternery
  const canDrink = (Number(answer) > 18) ? "get drinking" : "you can have a lemonade young man";

  console.log("You may drink: ", canDrink);
  process.exit(0);
});


